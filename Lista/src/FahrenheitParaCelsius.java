import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class FahrenheitParaCelsius {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double temperaturaCelsius;
        double temperaturaFahrenheit;

        System.out.print("Digite a temperatura em ºF: ");
        temperaturaFahrenheit = leia.nextDouble();

        temperaturaCelsius = (temperaturaFahrenheit - 32) * 5 / 9;

        System.out.format("%.2f ºF em Celsius: %.2f", temperaturaFahrenheit, temperaturaCelsius);
    }
}

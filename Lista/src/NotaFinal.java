import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class NotaFinal {
    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);
        
        double notaProvaEscrita; double notaProvaDesempenho; double notaProvaTitulos; double notaFinal;
        
        System.out.print("Digite a nota da prova escrita: ");
        notaProvaEscrita = leia.nextDouble();
        
        System.out.print("Digite a nota da prova de desempenho: ");
        notaProvaDesempenho = leia.nextDouble();
        
        System.out.print("Digite a nota da prova de títulos: ");
        notaProvaTitulos = leia.nextDouble();
        
        notaFinal = (0.4 * notaProvaEscrita) + (0.4 * notaProvaDesempenho) + (0.2 * notaProvaTitulos); 
        
        System.out.format("Nota final: %.2f", notaFinal);
        
    }
}

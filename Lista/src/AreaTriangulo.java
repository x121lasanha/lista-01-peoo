
import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class AreaTriangulo {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double base;
        double altura;
        double area;

        System.out.print("Digite o valor da base do triângulo: ");
        base = leia.nextDouble();

        System.out.print("Digite o valor da altura do triângulo: ");
        altura = leia.nextDouble();

        area = (base * altura) / 2;

        System.out.format("Área do triângulo: %.1f", area);
    }
}

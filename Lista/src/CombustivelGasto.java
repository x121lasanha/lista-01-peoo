
import java.util.Scanner;

public class CombustivelGasto {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double distanciaPercorrida;
        double tempoGasto;
        double velocidadeMedia;
        double litrosGastos;

        System.out.print("Digite o tempo gasto (em horas): ");
        tempoGasto = leia.nextDouble();

        System.out.print("Digite a velocidade média (km/h): ");
        velocidadeMedia = leia.nextDouble();

        distanciaPercorrida = tempoGasto * velocidadeMedia;

        litrosGastos = distanciaPercorrida / 12;

        System.out.format("Velocidade média: %.1fkm/h Tempo gasto: %.1fh Distância percorrida: %.1fkm Litros gastos: %.1fl",
                velocidadeMedia, tempoGasto, distanciaPercorrida, litrosGastos);

    }
}

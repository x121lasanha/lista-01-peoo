import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class PotenciaRaiz {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        int num;
        double potencia;
        double raiz;

        System.out.print("Digite um número: ");
        num = leia.nextInt();

        potencia = Math.pow(num, 2);
        raiz = Math.sqrt(num);

        System.out.format("Potencia: %f Raiz: %f", potencia, raiz);

    }
}

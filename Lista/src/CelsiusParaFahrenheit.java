import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class CelsiusParaFahrenheit {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double temperaturaCelsius;
        double temperaturaFahrenheit;

        System.out.print("Digite a temperatura em ºC: ");
        temperaturaCelsius = leia.nextDouble();

        temperaturaFahrenheit = ((9 * temperaturaCelsius) + 160) / 5;

        System.out.format("%.2f ºC em Fahrenheit: %.2f", temperaturaCelsius, temperaturaFahrenheit);

    }
}

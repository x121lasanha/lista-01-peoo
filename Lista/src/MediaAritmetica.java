
import java.util.Scanner;

public class MediaAritmetica {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        int nota1;
        int nota2;
        int nota3;
        int soma;
        int mediaAritmetica;

        System.out.print("Digite a primeira nota: ");
        nota1 = leia.nextInt();

        System.out.print("Digite a segunda nota: ");
        nota2 = leia.nextInt();

        System.out.print("Digite a terceira nota: ");
        nota3 = leia.nextInt();

        soma = nota1 + nota2 + nota3;
        mediaAritmetica = soma / 3;

        System.out.format("Sua média aritmética é: %d", mediaAritmetica);
    }
}

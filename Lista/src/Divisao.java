import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class Divisao {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double num1;
        double num2;
        double dividendo;
        double divisor;
        double quociente;
        double resto;

        System.out.print("Digite um número: ");
        num1 = leia.nextDouble();

        System.out.print("Digite outro número: ");
        num2 = leia.nextDouble();

        dividendo = num1;
        divisor = num2;
        quociente = dividendo / divisor;
        resto = dividendo % divisor;

        System.out.format("Dividendo: %.1f Divisor: %.1f Quociente: %.1f Resto: %.1f", dividendo, divisor, quociente, resto);

    }
}

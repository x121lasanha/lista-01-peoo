import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class Soma {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        int num1;
        int num2;
        int soma;

        System.out.print("Digite um número: ");
        num1 = leia.nextInt();

        System.out.print("Digite outro número: ");
        num2 = leia.nextInt();

        soma = num1 + num2;

        System.out.format("A soma é: %d", soma);
    }
}


import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class ValorPagoLanche {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        int coxinhas;
        int pasteis;
        int refrigerantes;
        double valorPago;
        double valorReferente;

        double precoCoxinha = 1.75;
        double precoPastel = 1.50;
        double precoRefrigerante = 2.50;

        System.out.print("Digite quantas coxinhas você comprou: ");
        coxinhas = leia.nextInt();

        System.out.print("Digite quantos pastéis você comprou: ");
        pasteis = leia.nextInt();

        System.out.print("Digite quantos refrigerantes você comprou: ");
        refrigerantes = leia.nextInt();

        valorPago = (coxinhas * precoCoxinha) + (pasteis * precoPastel) + (refrigerantes * precoRefrigerante);

        System.out.format("Valor total pago pelo lanche: R$%.2f", valorPago);

        valorReferente = valorPago * 0.05;

        System.out.format("\nValor referente a 5 por cento sobre o total: %.3f", valorReferente);

    }
}

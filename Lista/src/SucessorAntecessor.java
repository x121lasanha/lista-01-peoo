import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class SucessorAntecessor {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        int num;
        int numSucessor;
        int numAntecessor;

        System.out.print("Digite um número: ");
        num = leia.nextInt();

        numSucessor = num + 1;
        numAntecessor = num - 1;

        System.out.format("Sucessor: %d Antecessor: %d", numSucessor, numAntecessor);
    }
}

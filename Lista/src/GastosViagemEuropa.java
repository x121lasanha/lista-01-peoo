
import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class GastosViagemEuropa {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double valorGastoEuro;
        double valorEuroEmReais = 4.32;
        double valorFatura;

        System.out.print("Digite quanto você gastou em euro: ");
        valorGastoEuro = leia.nextDouble();

        valorFatura = valorGastoEuro * valorEuroEmReais;

        System.out.format("Valor para pagar na fatura: R$%.2f", valorFatura);

    }
}

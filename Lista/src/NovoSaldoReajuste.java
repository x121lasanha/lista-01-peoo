import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class NovoSaldoReajuste {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double saldo;
        double novoSaldo;

        System.out.print("Digite seu saldo bancário: ");
        saldo = leia.nextDouble();

        novoSaldo = saldo + (saldo * 0.075);

        System.out.println("Novo saldo com reajuste de 7.5% (aumento): " + novoSaldo);
    }
}

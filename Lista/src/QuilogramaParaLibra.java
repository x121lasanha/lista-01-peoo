
import java.util.Scanner;

/**
 *
 * @author Ideale
 */
public class QuilogramaParaLibra {

    public static void main(String[] args) {
        Scanner leia = new Scanner(System.in);

        double quilogramas;
        double libras;

        System.out.print("Digite um valor em quilogramas: ");
        quilogramas = leia.nextDouble();

        libras = quilogramas * 2.2;

        System.out.format("Valor em libras: %.2f", libras);
    }
}
